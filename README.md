# FixFuncRotating
This plugin fixes two **func_rotating`s** bugs<br />
Briefly about first bug, when we send _StopAtStartPos_ input to the func_rotating entity we expect rotating eventually to stop, but due to this incorrect check
```c++
if ( flNewSpeed <= 25 && fabs( angDelta ) < 1.0f )
```
our entity does not stop and keeps rotating with 20 speed.<br />
Note. You can observe this bug in _ze\_gris\_fyk_ map on the 2nd stage`s boss<br />
About second bug, assume that _StopAtStartPos_ works correctly, after this sequence of inputs to func_rotating<br /> ![alt text](https://i.postimg.cc/BnskJ82f/image.png)<br /> we expect it to rotate but it stops.
# SkyCameraActivate
This is implementation of **sky_camera** _Activate_ input that allow you to switch between different **sky_camera**<br />Example [here](https://www.youtube.com/watch?v=sGYYKPaJrSA)