#pragma semicolon 1

#define PLUGIN_AUTHOR "Cloud Strife"
#define PLUGIN_VERSION "1.0"

#include <sourcemod>
#include <sdktools>
#include <dhooks>
#define nullptr Address_Null

#pragma newdecls required

public Plugin myinfo = 
{
	name = "SkyCamera Activate",
	author = PLUGIN_AUTHOR,
	description = "sky_camera Activate input implementation",
	version = PLUGIN_VERSION,
	url = "https://steamcommunity.com/id/cloudstrifeua/"
};

Handle g_hAcceptInput = null;
Address g_pClassList = nullptr;
int g_pNextOffset = -1;
bool g_bLate = false;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLate = late;
	return APLRes_Success;
}

public void OnPluginStart()
{
	Handle hGameConf = LoadGameConfigFile("sdktools.games");
	if(hGameConf == INVALID_HANDLE)
	{
		SetFailState("Couldn't load sdktools game config!");
		return;
	}
	
	int Offset = GameConfGetOffset(hGameConf, "AcceptInput");
	g_hAcceptInput = DHookCreate(Offset, HookType_Entity, ReturnType_Bool, ThisPointer_CBaseEntity, AcceptInput);
	DHookAddParam(g_hAcceptInput, HookParamType_CharPtr);
	DHookAddParam(g_hAcceptInput, HookParamType_CBaseEntity);
	DHookAddParam(g_hAcceptInput, HookParamType_CBaseEntity);
	DHookAddParam(g_hAcceptInput, HookParamType_Object, 20, DHookPass_ByVal|DHookPass_ODTOR|DHookPass_OCTOR|DHookPass_OASSIGNOP); //varaint_t is a union of 12 (float[3]) plus two int type params 12 + 8 = 20
	DHookAddParam(g_hAcceptInput, HookParamType_Int);

	CloseHandle(hGameConf);
	
	hGameConf = LoadGameConfigFile("SkyCameraActivate.games");
	if(hGameConf == INVALID_HANDLE)
	{
		SetFailState("Couldn't load SkyCameraActivate game config!");
		return;
	}
	
	g_pNextOffset = GameConfGetOffset(hGameConf, "CSkyCamera::m_pNext");
	if(g_pNextOffset == -1)
	{
		SetFailState("Couldn't load CSkyCamera::m_pNext offset!");
		return;
	}
	
	g_pClassList = GameConfGetAddress(hGameConf, "CEntityClassList<CSkyCamera>::m_pClassList");
	if(!g_pClassList)
	{
		SetFailState("Couldn't load CEntityClassList<CSkyCamera>::m_pClassList address!");
		return;
	}
	
	if(g_bLate)
	{
		int startEnt = -1;
		while((startEnt = FindEntityByClassname(startEnt, "sky_camera"))!= -1)
		{
			if(IsValidEntity(startEnt))
			{
				OnEntityCreated(startEnt, "sky_camera");
			}
		}
	}
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if(StrEqual(classname, "sky_camera", false))
	{
		DHookEntity(g_hAcceptInput, false, entity);
	}
}

//Removes given sky_camera from the list
public void RemoveCamera(int camera)
{
	Address pCamera = GetEntityAddress(camera);
	Address pPrev = g_pClassList;
	Address pCur = view_as<Address>(LoadFromAddress(pPrev, NumberType_Int32));
	while(pCur != nullptr)
	{
		Address pCurNext = pCur + view_as<Address>(g_pNextOffset);
		if(pCur == pCamera)
		{
			StoreToAddress(pPrev, LoadFromAddress(pCurNext, NumberType_Int32), NumberType_Int32);
			return;
		}
		pPrev = pCurNext;
		pCur = view_as<Address>(LoadFromAddress(pPrev, NumberType_Int32));
	}
}

//Inserts given sky_camera to the beginning of the list
public void InsertCamera(int camera)
{
	Address pCamera = GetEntityAddress(camera);
	StoreToAddress(pCamera + view_as<Address>(g_pNextOffset), LoadFromAddress(g_pClassList, NumberType_Int32), NumberType_Int32);
	StoreToAddress(g_pClassList, view_as<int>(pCamera), NumberType_Int32);
}

public MRESReturn AcceptInput(int camera, Handle hReturn, Handle hParams)
{
	if(!IsValidEntity(camera))
		return MRES_Ignored;
		
	char sCommand[32];
	DHookGetParamString(hParams, 1, sCommand, sizeof(sCommand));
	if(!StrEqual(sCommand, "Activate", false))
		return MRES_Ignored;
		
	RemoveCamera(camera);
	InsertCamera(camera);
	
	DHookSetReturn(hReturn, 1);
	return MRES_Supercede;
}